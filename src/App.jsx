import React from "react";
import Home from "./pages/Home";
import Gallery from "./pages/Gallery";
import MovieDetails from "./pages/MovieDetails";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/gallery" exact component={Gallery} />
        <Route exact path="/details/:movieId" component={MovieDetails} />
      </Switch>
    </Router>
  );
}

export default App;
