/* import MoviesGrid from "../components/MoviesGrid"; */
import Navigation from "../components/Navigation";
import Slider from "../components/Slider";

function Home() {
  return (
    <div>
      <Slider />
      <Navigation />
    </div>
  );
}

export default Home;
