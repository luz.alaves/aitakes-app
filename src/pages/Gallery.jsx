import MoviesGrid from "../components/MoviesGrid";
import Navigation from "../components/Navigation";
import styles from "../css/App.module.css";

function Gallery() {
  return (
    <div>
      <header>
        <h1 className={styles.title}>Movies</h1>
      </header>
      <main>
        <MoviesGrid />
        <Navigation />
      </main>
    </div>
  );
}

export default Gallery;
