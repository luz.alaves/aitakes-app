import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import styles from "../css/Slider.module.css";

function Slider() {
  const baseUrl = "http://react-responsive-carousel.js.org/assets/";
  const datas = [
    {
      id: 1,
      image: `${baseUrl}4.jpeg`,
      title: "Titre du slider 1",
      text: `Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
                  Quo minima voluptatibus distinctio, enim laborum atque odio, 
                  accusamus ipsam non sapiente consectetur molestias beatae fugit necessitatibus. 
                  Facilis aut sed modi aliquam.`,
    },
    {
      id: 2,
      image: `${baseUrl}5.jpeg`,
      title: "Titre du slider 2",
      text: `Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
                  Quo minima voluptatibus distinctio, enim laborum atque odio, 
                  accusamus ipsam non sapiente consectetur molestias beatae fugit necessitatibus. 
                  Facilis aut sed modi aliquam.`,
    },
    {
      id: 3,
      image: `${baseUrl}6.jpeg`,
      title: "Titre du slider 3",
      text: `Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
                  Quo minima voluptatibus distinctio, enim laborum atque odio, 
                  accusamus ipsam non sapiente consectetur molestias beatae fugit necessitatibus. 
                  Facilis aut sed modi aliquam.`,
    },
  ];
  return (
    <Carousel
      className={styles.slideSize}
      autoPlay
      interval={3000}
      infiniteLoop
      showIndicators={false}
      showStatus={false}
    >
      {datas.map((slide) => (
        <div key={slide.id}>
          <img src={slide.image} alt="" />
          <div>
            <h2 className={styles.overlay__title}>{slide.title}</h2>
            <p className={styles.overlay__text}>{slide.text}</p>
          </div>
        </div>
      ))}
    </Carousel>
  );
}

export default Slider;
