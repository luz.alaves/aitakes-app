import { Link } from "react-router-dom";
import styles from "../css/Navigation.module.css";

function Navigation() {
  return (
    <footer className={styles.footer}>
      <Link exact to="/" className="btn btn-primary">
        home
      </Link>
      <p>
        <strong>NAVIGATION</strong>
      </p>
      <Link exact to="/gallery" className="btn btn-primary">
        liste
      </Link>
    </footer>
  );
}

export default Navigation;
