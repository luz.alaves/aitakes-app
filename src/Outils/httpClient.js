const API = "https://api.themoviedb.org/3";

function get(path) {
    return fetch(API + path, {
        headers: {
        Authorization:
            "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIyNjE3NjVhMjFiODVjNWM0MzljZTU0YTFkYWZhYzNhYyIsInN1YiI6IjYwZWI3Zjg2NTNmODMzMDA0NWVjNmZiZSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.b-YNwKOHhoJ8K1SHuhNCe3lVhgNp4qCMmI_wYZG-zOI",
            "Content-Type": "application/json;charset=utf-8",
        },
    })
        .then((result) => result.json());
}

export default get;